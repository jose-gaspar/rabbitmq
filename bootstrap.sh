#!/usr/bin/env bash

# install epel-release
sudo rpm --import https://dl.fedoraproject.org/pub/epel/RPM-GPG-KEY-EPEL-7
sudo yum install -y epel-release

# configure rabbitmq-erlang repo
cat >> /etc/yum.repos.d/rabbitmq-erlang.repo <<EOL
[rabbitmq-erlang]
name=rabbitmq-erlang
baseurl=https://dl.bintray.com/rabbitmq/rpm/erlang/20/el/7
gpgcheck=1
gpgkey=https://dl.bintray.com/rabbitmq/Keys/rabbitmq-release-signing-key.asc
repo_gpgcheck=0
enabled=1
EOL

rpm --import https://dl.bintray.com/rabbitmq/Keys/rabbitmq-release-signing-key.asc

# install dependencies
sudo yum install -y erlang

# download rabbitmq version 3.2.4
curl -O https://www.rabbitmq.com/releases/rabbitmq-server/v3.2.4/rabbitmq-server-3.2.4-1.noarch.rpm

# install rabbitmq and start the service
sudo yum install -y rabbitmq-server-3.2.4-1.noarch.rpm
sudo systemctl enable rabbitmq-server
sudo systemctl start rabbitmq-server

# enable rabbitmq management plugin (optional for providing access to Web UI)
sudo rabbitmq-plugins enable rabbitmq_management
sudo systemctl restart rabbitmq-server

# install nodejs and amqp-tool
sudo yum install -y npm
sudo npm install amqp-tool -g

# install rabbitmqadmin
curl -O http://localhost:15672/cli/rabbitmqadmin
chmod +x rabbitmqadmin
sudo mv rabbitmqadmin /usr/local/bin/rabbitmqadmin
