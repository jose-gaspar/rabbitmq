### Vagrant box with rabbitmq 3.2.4 and amqp-tool as a cli for importing and/or exporting message from/to an AMQP/RabbitMQ broker

add a rabbitmq user with a password and set required permissions

    $ sudo rabbitmqctl add_user jgaspar test2
    $ sudo rabbitmqctl set_user_tags jgaspar administrator
    $ sudo rabbitmqctl set_permissions -p "/" jgaspar ".*" ".*" ".*"

export the first message of a queue

    $ amqp-tool -u jgaspar -p test2 -q test --count 1 --export dump.json

import all messages from a file into a queue

    $ amqp-tool -u jgaspar -p test2 -q test --import dump.json

more examples from official github repo

https://github.com/FGRibreau/node-amqp-tool

messages can be retrieved in a non-destructive and destructive way using `rabbitmqadmin`

    $ curl -O http://localhost:15672/cli/rabbitmqadmin; chmod +x rabbitmqadmin; sudo mv rabbitmqadmin /usr/local/bin/rabbitmqadmin
    $ rabbitmqadmin -u jgaspar -p test2 get queue=test count=2                  #non-destructive
    $ rabbitmqadmin -u jgaspar -p test2 get queue=test requeue=false count=2    #destructive


